<?php
require_once "classes/LoadPurlDataBase.class.php";
require_once "classes/dao/purls/PurlsDAOFactory.php";

class LoadPurlData extends LoadPurlDataBase
{
    public function format($data, $request)    {
	 $videoData = array(
	  "shipping-first-name" => "",
	  "shipping-last-name" => "",
	  "shipping-address-line1" => "",
	  "shipping-address-line2" => "",
	  "shipping-address-line3" => "",
	  "shipping-city" => "",
	  "shipping-region" => "",
	  "shipping-postal-code" => "",
	  "shipping-country" => "",  
	  "EFX" => "",  
	  "language" => "",
	  "currency" => "",
	  "total-sale-price" => "",
	  "shipping-and-handling" => "",
	  "shipping-time" => "",
	  "duties-and-taxes" => "",
	  "order-total" => "",
	  "shipping-methods-available-count" => "",
	  "shipping-methods-available1" => "",
	  "shipping-methods-available2" => "",
	  "shipping-methods-available3" => "",
	  "shipping-methods-available4" => "",
	  "second_visit" => ""
	 );    
	 
	 $videoData = array_merge($data, $videoData);
	 
	 $params = json_decode($request['otherParams'], true);
	 
	 //replace values from url
	 foreach($params as $key => $value){
	      foreach($videoData as $vdKey => $vdValue){
            if($key == $vdKey)
              $vdValue = $value;
	      }
	 }	 
	 
	 $videoData = array_merge($videoData, $params);
	 
	 return $videoData;
    }
    
    public function get($request, $config = null)    {
    
	if (!isset($request['uid'])) {
            if ($config === null || ! $config->isOrganicSupported()) {
                return false;
            }	    
            return self::format($this->visitData, $request);
        }

        $row = PurlsDAOFactory::getFactory()->load($request['uid']);

        if ($row == null) {
            if (strpos($request['uid'], ".") === false) {
                tidyUpDb();

                return false;
            }

            $row = PurlsDAOFactory::getFactory()->load(rtrim($request['uid'], "."));

            if ($row == null) {
                tidyUpDb();

                return false;
            }
        }

        if (self::hasPurlExpired($row)) {
            return "expired";
        }

        tidyUpDb();

        if (($ret = self::decodeJSON($row['data'], $request['uid'])) === false) {
            return false;
        }

	$ret = array_merge($ret, $this->visitData);

	return self::format($ret, $request);
    }
}

?>